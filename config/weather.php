<?php

/**
 * Open weather api related data
*/

return [
    "cities"=>[
        [
            "id" => 1248991,
            "name" => "Colombo"
        ],
        [
            "id" => 4163971,
            "name" => "Melbourne"
        ]
    ],

    'open_weather_app_id' => env('OPEN_WHETHER_API_ID', ''),
    'open_weather_app_url' => env('OPEN_WHETHER_API_URL', ''),
   
];
