# Getting started

## Installation

Please check the official Laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/8.x/installation)

PHP 7.4 + version
MySql 5.7
 

Clone the repository

    git clone <git url>

Switch to the repo folder

    cd temperatures

Install all the dependencies using composer

    composer install

Install npm package manager

    npm install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Vuejs app build (JetStream)

    npm run dev

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000



## Installation

Laravel JetStream - for authentication and UI with Inertia + Vue

Laravel Telescope  - [Access Url](http://localhost:8000/telescope/requests)

Telescope makes a wonderful companion to your local Laravel development environment. Telescope provides insight into the requests coming into your application, exceptions, log entries, database queries, queued jobs, mail, notifications, cache operations, scheduled tasks, variable dumps, and more.
 [Official Documentation](https://laravel.com/docs/8.x/telescope#introduction)

Laravel guzzle - for HTTP Client

Laravel provides an expressive, minimal API around the Guzzle HTTP client, allowing you to quickly make outgoing HTTP requests to communicate with other web applications. Laravel's wrapper around Guzzle is focused on its most common use cases and a wonderful developer experience.

Open weather data access via api.