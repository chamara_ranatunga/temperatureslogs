<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTemperaturesLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_temperatures_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->integer('city_id')->index('city_id');
            $table->decimal('celsius', 5,2)->index('celsius');
            $table->decimal('fahrenheit', 6,2)->index('fahrenheit');             
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_temperatures_logs', function (Blueprint $table) {      
            $table->dropIndex('city_id');                    
            $table->dropIndex('celsius');                    
            $table->dropIndex('fahrenheit');   
            $table->dropForeign(['user_id']);            
        });

        Schema::dropIfExists('user_temperatures_logs');
    }
}
