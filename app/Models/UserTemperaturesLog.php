<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTemperaturesLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'city_id', 'celsius', 'fahrenheit', 'created_at', 'updated_at'
    ];


    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('l, d M Y, g:i a');
    }
    
}
