<?php

namespace App\Repository\UserLogs;

use DB;
use Auth;
use Carbon\Carbon;
use App\Models\UserTemperaturesLog;
use App\Services\OpenWeatherService;
use App\Traits\TemperatureConversion;
use App\Repository\UserLogs\Contracts\WeatherRepositoryInterface;

class WeatherRepository implements WeatherRepositoryInterface
{

    /**
     * getCustomerList
     *
     * @param  Int $cityId

     * @return void
     */
    public function getUserTemperaturesList($cityId)
    {
        try {
            $user = Auth::user();
            $logs = UserTemperaturesLog::select('city_id', 'celsius', 'fahrenheit', 'created_at')
                                        ->where('user_id', $user->id)                              
                                        ->where('city_id', $cityId);  
                                        
            if(request()->has(['order'])){
                $logs =  $logs->orderBy('celsius', 'DESC');
            }else {
                $logs =  $logs->orderBy('created_at', 'DESC');
            }                    
            return $logs->paginate(10);

        } catch (\Throwable $th) {           
            DB::rollback();
            \Log::error($th);
            abort(500, 'Something went wrong please contact administrator');
        }
    }
       
  
        
    /**
     * create wether logs 
     *
     * @return mix
     */
    public function addWeatherLog()
    {
        try {
            $payload = $this->getCitesData();

            if(count($payload) > 0){
                $log = UserTemperaturesLog::insert($payload);  
            }else{
                abort(404, 'Open wether data not available');
            }                     
           
        } catch (\Throwable $th) {           
            \Log::error($th);
            abort(500, 'Could not create logs, please try again later');
        }
    }      


    private function getCitesData()
    {
        $cites = config('weather.cities');
        $weatherLog = [];
        $user = Auth::user();
        foreach ($cites as $city) {
            $openWeather = new OpenWeatherService();
            $temp = $openWeather->getCurrentWeatherByCity($city['id']);
            if($temp){   

                $weatherLog[] = [                   
                    'user_id' => $user->id,
                    'city_id' => $city['id'],
                    'celsius' => $temp["main"] ? $temp["main"]["temp"] ? TemperatureConversion::kelvinToCelsius($temp["main"]["temp"]): 0 : 0,
                    'fahrenheit' => $temp["main"] ? $temp["main"]["temp"] ? TemperatureConversion::kelvinToFahrenheit($temp["main"]["temp"]): 0 : 0,
                    'created_at' => Carbon::now()
                ];
            }          
        }
       return $weatherLog;
    }
   
}
