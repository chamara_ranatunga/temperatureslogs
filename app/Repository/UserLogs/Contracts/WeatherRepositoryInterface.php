<?php

namespace App\Repository\UserLogs\Contracts;

interface WeatherRepositoryInterface
{
    /**
     * get temperature log List
     *
     * @param  mixed $keyword
     * @return void
     */
    public function getUserTemperaturesList($cityId);
    
    /**
     * add temperature logs
     *
     * @return void
     */
    public function addWeatherLog();
    
   

}
