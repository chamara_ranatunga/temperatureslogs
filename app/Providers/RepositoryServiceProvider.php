<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repository\UserLogs\WeatherRepository;
use App\Repository\UserLogs\Contracts\WeatherRepositoryInterface;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(WeatherRepositoryInterface::class, WeatherRepository::class);
    }
}


?>