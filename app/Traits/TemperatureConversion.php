<?php

namespace App\Traits;


trait TemperatureConversion {

   public static function kelvinToCelsius($value)
   {       
        return number_format(($value - 273.15),0);

   }


   public static function kelvinToFahrenheit($value)
   {        
        return number_format((($value - 273.15) * 9/5 + 32),0);
   }


   


   
}