<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Repository\UserLogs\Contracts\WeatherRepositoryInterface;

class DashboardController extends Controller
{
    /**
     * @param WeatherRepositoryInterface $weatherRepository
    */    
   
    /**
     *
     * @return void
     */
    public function __construct(WeatherRepositoryInterface $weatherRepository) {

        $this->weatherRepository = $weatherRepository;
    }

    /**
     * Dashboard data
    */

    public function index()
    {       
        $cites = config('weather.cities');
        
        foreach ($cites as $key => $city) {
            $logs[$key]['logs'] = $this->weatherRepository->getUserTemperaturesList($city['id']);
            $logs[$key]['name'] = $city['name'];
        }

        return Inertia::render('Dashboard', [
            'logs' => $logs,
        ]);
    }
}
