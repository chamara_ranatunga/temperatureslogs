<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Repository\UserLogs\Contracts\WeatherRepositoryInterface;

class LoginListener
{
    /**
     * @param WeatherRepositoryInterface $weatherRepository
    */    
   
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(WeatherRepositoryInterface $weatherRepository) {

        $this->weatherRepository = $weatherRepository;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->weatherRepository->addWeatherLog();
    }
}
