<?php
namespace App\Services;

use Illuminate\Support\Facades\Http;

class OpenWeatherService
{    
    protected $apiKey;
    
    /**
     * CommonRepositoryInterface constructor.
     * @param CommonRepositoryInterface $commonRepository
     */
    public function __construct() {
        $this->apiKey = config('weather.open_weather_app_id');
        $this->apiUrl = config('weather.open_weather_app_url');
    }
    /**
     * get Current Weather By City Id
     *
     * @param  Int $cityId
     * @return JsonResponse
     */
    public function getCurrentWeatherByCity($cityId)
    {
        $param = "id=".$cityId."&appid=".$this->apiKey;    
        
        return Http::get($this->apiUrl.$param);
    }

}




?>